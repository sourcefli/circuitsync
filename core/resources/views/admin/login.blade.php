<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
  	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <title>{{$bs->website_title}}</title>
  	<link rel="icon" href="{{asset('assets/front/img/favicon.jpg')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/login.css')}}">
  </head>
  <body>
    <div class="login-page">
      <div class="text-center mb-4">
        <img class="login-logo" src="{{asset('assets/front/img/logo.png')}}" alt="">
      </div>
      <div class="form">
        @if (session()->has('alert'))
          <div class="alert alert-danger fade show" role="alert" style="font-size: 14px;">
            <strong>Oops!</strong> {{session('alert')}}
          </div>
        @endif
        <form class="login-form" action="{{route('admin.auth')}}" method="POST">
          @csrf
          <input type="text" name="username" placeholder="username"/>
          @if ($errors->has('username'))
            <p class="text-danger text-left">{{$errors->first('username')}}</p>
          @endif
          <input type="password" name="password" placeholder="password"/>
          @if ($errors->has('password'))
            <p class="text-danger text-left">{{$errors->first('password')}}</p>
          @endif
          <button type="submit">login</button>
        </form>
      </div>
    </div>
  </body>
</html>
