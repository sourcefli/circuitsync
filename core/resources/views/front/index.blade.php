@extends('front.layout')

@section('content')
  <!--   hero area start   -->
  <div class="hero-area hero-bg ">
     <div class="container">
        <div class="hero-txt">
           <div class="row">
              <div class="col-12">
                 <span>{{$bs->hero_section_title}}</span>
                 <h1>{{$bs->hero_section_text}}</h1>
                 <a href="{{$bs->hero_section_button_url}}" class="hero-boxed-btn" target="_blank">{{$bs->hero_section_button_text}}</a>
              </div>
           </div>
        </div>
     </div>
     <div class="hero-area-overlay"></div>
  </div>
  <!--   hero area end    -->


  <!--    introduction area start   -->
  <div class="intro-section">
     <div class="container">
        <div class="hero-features">
           <div class="row">
              @foreach ($features as $key => $feature)
                <div class="col-md-3 col-sm-6 single-hero-feature">
                   <div class="outer-container">
                      <div class="inner-container">
                         <div class="icon-wrapper">
                            <i class="{{$feature->icon}}"></i>
                         </div>
                         <h3>{{$feature->title}}</h3>
                      </div>
                   </div>
                </div>
              @endforeach
           </div>
        </div>
        <div class="row">
           <div class="col-lg-6 pr-0">
              <div class="intro-txt">
                 <span class="section-title">{{$bs->intro_section_title}}</span>
                 <h2 class="section-summary">{{$bs->intro_section_text}} </h2>
                 <a href="{{$bs->intro_section_button_url}}" class="intro-btn" target="_blank"><span>{{$bs->intro_section_button_text}}</span></a>
              </div>
           </div>
           <div class="col-lg-6 pl-lg-0 px-md-3 px-0">
              <div class="intro-bg">
                <a id="play-video" class="video-play-button" href="{{$bs->intro_section_video_link}}">
                  <span></span>
                </a>
              </div>
           </div>
        </div>
     </div>
  </div>
  <!--    introduction area end   -->


  <!--   service section start   -->
  <div class="service-categories">
    <div class="container">
       <div class="row text-center">
          <div class="col-lg-6 offset-lg-3">
             <span class="section-title">{{$bs->service_section_title}}</span>
             <h2 class="section-summary">{{$bs->service_section_subtitle}}</h2>
          </div>
       </div>
    </div>
    <div class="container">
      <div class="row">
        @foreach ($scats as $key => $scat)
          <div class="col-xl-3 col-lg-4 col-sm-6">
            <div class="single-category">
              <div class="img-wrapper">
                <img src="{{asset('assets/front/img/service_category_icons/'.$scat->image)}}" alt="">
              </div>
              <div class="text">
                <h4>{{$scat->name}}</h4>
                <p>{{$scat->short_text}}</p>
                <a href="{{route('front.services', ['category'=>$scat->id])}}" class="readmore">Read More</a>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </div>
  <!--   service section end   -->


  <!--   how we do section start   -->
  <div class="approach-section">
     <div class="container">
        <div class="row">
           <div class="col-lg-6">
              <div class="approach-summary">
                 <span class="section-title">{{$bs->approach_title}}</span>
                 <h2 class="section-summary">{{$bs->approach_subtitle}}</h2>
                 <a href="{{$bs->approach_button_url}}" class="boxed-btn" target="_blank"><span>{{$bs->approach_button_text}}</span></a>
              </div>
           </div>
           <div class="col-lg-6">
              <ul class="approach-lists">
                 @foreach ($points as $key => $point)
                   <li class="single-approach">
                      <div class="approach-icon-wrapper"><i class="{{$point->icon}}"></i></div>
                      <div class="approach-text">
                         <h4>{{$point->title}}</h4>
                         <p>{{$point->short_text}}</p>
                      </div>
                   </li>
                 @endforeach
              </ul>
           </div>
        </div>
     </div>
  </div>
  <!--   how we do section end   -->


  <!--    call to action section start    -->
  <div class="cta-section cta-bg">
     <div class="container">
        <div class="cta-content">
           <div class="row">
              <div class="col-md-9 col-lg-7">
                 <h3>{{$bs->cta_section_text}}</h3>
              </div>
              <div class="col-md-3 col-lg-5 contact-btn-wrapper">
                 <a href="{{$bs->cta_section_button_url}}" class="boxed-btn contact-btn"><span>{{$bs->cta_section_button_text}}</span></a>
              </div>
           </div>
        </div>
     </div>
     <div class="cta-overlay"></div>
  </div>
  <!--    call to action section end    -->


  <!--    case section start   -->
  <div class="case-section">
     <div class="container">
        <div class="row text-center">
           <div class="col-lg-6 offset-lg-3">
              <span class="section-title">{{$bs->portfolio_section_title}}</span>
              <h2 class="section-summary">{{$bs->portfolio_section_text}}</h2>
           </div>
        </div>
     </div>
     <div class="container-fluid">
        <div class="row">
           <div class="col-md-12">
              <div class="case-carousel owl-carousel owl-theme">
                 @foreach ($portfolios as $key => $portfolio)
                   <div class="single-case single-case-bg-1" style="background-image: url('{{asset('assets/front/img/portfolios/featured/'.$portfolio->featured_image)}}');">
                      <div class="outer-container">
                         <div class="inner-container">
                            <h4>{{strlen($portfolio->title) > 36 ? substr($portfolio->title, 0, 36) . '...' : $portfolio->title}}</h4>
                            @if (!empty($portfolio->service))
                            <p>{{$portfolio->service->title}}</p>
                            @endif
                            <a href="{{route('front.portfoliodetails', $portfolio->slug)}}" class="readmore-btn"><span>View More</span></a>
                         </div>
                      </div>
                   </div>
                 @endforeach
              </div>
           </div>
        </div>
     </div>
  </div>
  <!--    case section end   -->

  <!--   Testimonial section start    -->
  <div class="testimonial-section pb-115">
     <div class="container">
        <div class="row text-center">
           <div class="col-lg-6 offset-lg-3">
              <span class="section-title">{{$bs->testimonial_title}}</span>
              <h2 class="section-summary">{{$bs->testimonial_subtitle}}</h2>
           </div>
        </div>
        <div class="row">
           <div class="col-md-12">
              <div class="testimonial-carousel owl-carousel owl-theme">
                 @foreach ($testimonials as $key => $testimonial)
                   <div class="single-testimonial">
                      <div class="img-wrapper"><img src="{{asset('assets/front/img/testimonials/'.$testimonial->image)}}" alt=""></div>
                      <div class="client-desc">
                         <p class="comment">{{$testimonial->comment}}</p>
                         <h6 class="name">{{$testimonial->name}}</h6>
                         <p class="rank">{{$testimonial->rank}}</p>
                      </div>
                   </div>
                 @endforeach
              </div>
           </div>
        </div>
     </div>
  </div>
  <!--   Testimonial section end    -->


  <!--    team section start   -->
  <div class="team-section section-padding team-bg">
     <div class="team-content">
        <div class="container">
           <div class="row text-center">
              <div class="col-lg-6 offset-lg-3">
                 <span class="section-title">{{$bs->team_section_title}}</span>
                 <h2 class="section-summary">{{$bs->team_section_subtitle}}</h2>
              </div>
           </div>
           <div class="row">
              <div class="team-carousel common-carousel owl-carousel owl-theme">
                @foreach ($members as $key => $member)
                 <div class="single-team-member">
                    <div class="team-img-wrapper">
                       <img src="{{asset('assets/front/img/members/'.$member->image)}}" alt="">
                       <div class="social-accounts">
                          <ul class="social-account-lists">
                             @if (!empty($member->facebook))
                               <li class="single-social-account"><a href="{{$member->facebook}}"><i class="fab fa-facebook-f"></i></a></li>
                             @endif
                             @if (!empty($member->twitter))
                               <li class="single-social-account"><a href="{{$member->twitter}}"><i class="fab fa-twitter"></i></a></li>
                             @endif
                             @if (!empty($member->linkedin))
                               <li class="single-social-account"><a href="{{$member->linkedin}}"><i class="fab fa-linkedin-in"></i></a></li>
                             @endif
                             @if (!empty($member->instagram))
                               <li class="single-social-account"><a href="{{$member->instagram}}"><i class="fab fa-instagram"></i></a></li>
                             @endif
                          </ul>
                       </div>
                    </div>
                    <div class="member-info">
                       <h5 class="member-name">{{$member->name}}</h5>
                       <small>{{$member->rank}}</small>
                    </div>
                 </div>
                @endforeach
              </div>
           </div>
        </div>
     </div>
     <div class="team-overlay"></div>
  </div>
  <!--    team section end   -->


  <!--    blog section start   -->
  <div class="blog-section section-padding">
     <div class="container">
        <div class="row text-center">
           <div class="col-lg-6 offset-lg-3">
              <span class="section-title">{{$bs->blog_section_title}}</span>
              <h2 class="section-summary">{{$bs->blog_section_subtitle}}</h2>
           </div>
        </div>
        <div class="blog-carousel owl-carousel owl-theme common-carousel">
           @foreach ($blogs as $key => $blog)
              <div class="single-blog">
                 <div class="blog-img-wrapper">
                    <img src="{{asset('assets/front/img/blogs/'.$blog->main_image)}}" alt="">
                 </div>
                 <div class="blog-txt">
                    <p class="date"><small>By <span class="username">Admin</span></small> | <small>{{date ( 'd M, Y', strtotime($blog->created_at) )}}</small> </p>
                    <h4 class="blog-title"><a href="{{route('front.blogdetails', $blog->slug)}}">{{strlen($blog->title) > 40 ? substr($blog->title, 0, 40) . '...' : $blog->title}}</a></h4>
                    <p class="blog-summary">{!! (strlen(strip_tags($blog->content)) > 100) ? substr(strip_tags($blog->content), 0, 100) . '...' : strip_tags($blog->content) !!}</p>
                    <a href="{{route('front.blogdetails', $blog->slug)}}" class="readmore-btn"><span>Read More</span></a>
                 </div>
              </div>
           @endforeach
        </div>
     </div>
  </div>
  <!--    blog section end   -->


  <!--   partner section start    -->
  <div class="partner-section">
     <div class="container top-border">
        <div class="row">
           <div class="col-md-12">
              <div class="partner-carousel owl-carousel owl-theme common-carousel">
                 @foreach ($partners as $key => $partner)
                   <a class="single-partner-item d-block" href="{{$partner->url}}" target="_blank">
                      <div class="outer-container">
                         <div class="inner-container">
                            <img src="{{asset('assets/front/img/partners/'.$partner->image)}}" alt="">
                         </div>
                      </div>
                   </a>
                 @endforeach
              </div>
           </div>
        </div>
     </div>
  </div>
  <!--   partner section end    -->
@endsection
